﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Timers;

namespace OnAir
{
    class Program
    {
        #region ChannelId
        static string ChannelId = "UCiYmuguRhWZVt2PmzaqyI0A";
        #endregion

        #region APIKey
        static string APIKey = "";
        #endregion

        static string url;

        static Timer LiveTimer;

        static void Main(string[] args)
        {
            // Get Live Status of YouTube Stream
            // if it is on-air set Raspberry Pi to light up sign
            // if it is off-air set Raspberry Pi to turn off sign

            /*
                https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=UCXswCcAMb5bvEUIDEzXFGYg&type=video&eventType=live&key=[API_KEY]
            */

            // create the URL for My Channel

            GetAPIKey(); // Read the API Key from the file apikey.txt in the DLL's folder

            url = @"https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=" + ChannelId + "&type=video&eventType=live&key=" + APIKey;
            Console.WriteLine("URL: " + url);
            LiveTimer = new Timer(10000);
            LiveTimer.Elapsed += LiveTimer_Elapsed;
            LiveTimer.AutoReset = true;
            LiveTimer.Enabled = true;

            Console.WriteLine("Press the Enter key to exit anytime... ");
            Console.ReadLine();

        }

        static void LiveTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //Console.WriteLine("Raised: {0} | Key: {1}", e.SignalTime, APIKey);
            // Get Data From YouTube            
            HttpWebRequest Request = (HttpWebRequest)WebRequest.Create(url);
            Request.Method = "GET";
            Request.KeepAlive = true;
            try
            {
                HttpWebResponse Response = (HttpWebResponse)Request.GetResponse();

                if (Response.StatusCode == HttpStatusCode.OK)
                {
                    var encoding = Encoding.ASCII;
                    using (var reader = new System.IO.StreamReader(Response.GetResponseStream(), encoding))
                    {
                        string responseText = reader.ReadToEnd();
                        Console.WriteLine(responseText);
                        YouTubeResponse results = JsonConvert.DeserializeObject<YouTubeResponse>(responseText);
                        if(results.items.Length == 0)
                        {
                            // Offline
                            Console.WriteLine("User Is Not Currently Live");
                        } else
                        {
                            // Online
                            Console.WriteLine("User Is Live!");
                        }
                    }
                
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }

        static void GetAPIKey()
        {
            APIKey = File.ReadAllText("apikey.txt");
        }
    }
}
